// 1.
db.users.find({$or: [{firstName: "s"}, {lastName: "d"}]},{
	firstName: 1, lastName: 1, _id: 0
});

// 2.
db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}]});

// 3.
db.users.find({$and: [{firstName: {$regex: "e"}}, {age: {$lte: 30}}]});